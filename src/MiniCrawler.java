import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class MiniCrawler {

	private static final String TARGET_ELEMENT_ID = "make-everything-ok-button";
//	private static final String ORIGIN = "c:/TestTasks/sample-0-origin.html";
//	private static final String OTHER = "c:/TestTasks/sample-4-the-mash.html";

	public static void main(String[] args) throws IOException {
		System.out.println("Starting ...");
		if (args == null || args.length == 0) {
			System.out.println("First parameter not found.");
			return;
		}
		String originFilePath = args[0];
		ArrayList<String> otherFilesPathes = new ArrayList<>();
		for (int i = 1; i < args.length; i++) {
			otherFilesPathes.add(args[i]);
		}
		if (otherFilesPathes.isEmpty()) {
			System.out.println("Other parameters not found.");
			return;
		}
//		String originFilePath = ORIGIN;
//		ArrayList<String> otherFilesPathes = new ArrayList<>();
//		otherFilesPathes.add(OTHER);

		String originOkButtonPath = getOkButtonPath(originFilePath, TARGET_ELEMENT_ID);
		ArrayList<String> otherFilesOkButtonPathes = new ArrayList<>();
		for (String path : otherFilesPathes) {
			otherFilesOkButtonPathes.add(getOkButtonPath(path, TARGET_ELEMENT_ID));
		}

		System.out.println("Path to the element on origin page [" + originOkButtonPath + "]");
		for (String otherPath : otherFilesOkButtonPathes) {
			System.out.println("Path to the element on other page [" + otherPath + "]");
		}
	}

	private static String getOkButtonPath(String originFilePath, String targetElementId) throws IOException {
		Document doc = Jsoup.parse(new File(originFilePath), "UTF-8");
		Element okButton = doc.getElementById(targetElementId);

		if (okButton == null) {
			return "Button with id [" + targetElementId + "] not found";
		}
		String okButtonPath = getElementPath(okButton.parent()) + okButton.nodeName() + " [" + okButton.id() + "]";

		return okButtonPath;
	}

	private static String getElementPath(Element element) {
		ArrayList<String> nodeNames = new ArrayList<String>();  
		StringBuilder sb = new StringBuilder();
		while(element.hasParent()) {
			nodeNames.add(0, element.normalName() + (element.id().isEmpty() ? "" : " [" + element.id() + "]"));
			element = element.parent();
		}
		for (String part : nodeNames) {
			sb.append(part);
			sb.append(" -> ");
		}
		return sb.toString();
	}
}
